#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
Application: StreamingAudit
Module: application starting point
description: Tasting opencv; learning ML/DL
author: Adriano Vieira <adriano.svieira at google.com>
character encoding: UTF-8
'''

import argparse
import yaml
from video import capture
from training import train
from audit import validate
import logging


try:
    CONFIG = yaml.safe_load(open('./config/config.yml'))
except Exception as error:
    logging.error('Trying to open application configuration file.\n%s' % error)
    raise

try:
    BRANDS = yaml.safe_load(open('./config/brands-dictionary.yml'))
except Exception as error:
    logging.error('Trying to open brands dictionary file.\n%s' % error)
    raise

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Streaming Audit')
    parser_capture = parser.add_argument_group('capture', 'Options to capture images of brands')
    #parser_train = parser.add_argument_group('train', 'Options to train models of brands')
    parser_audit = parser.add_argument_group('audit', 'Options to audit trained models of brands')

    parser.add_argument('--action', required=True,
                        dest='ACTION',
                        metavar='capture',
                        choices=['capture', 'train', 'audit'],
                        help='action to be performed \
                              [train, capture or audit]')

    # group options capture
    parser_capture.add_argument('--brand',
                                dest='BRAND',
                                metavar='tv-brasil',
                                default='tv-brasil',
                                help='brand to perform action \
                                [see: config/brands-dictionary.yml] (default: tv-brasil)')
    parser_capture.add_argument('-fc', '--frames_count',
                                dest='FRAMES_COUNT',
                                metavar='N',
                                type=int,
                                default=CONFIG['capture']['frames_count'],
                                help='N Frames to capture images/brands \
                                [see: config/config.yml] (default: %s)'
                                % CONFIG['capture']['frames_count'])
    parser_capture.add_argument('--all-brands',
                                dest='ALL_BRANDS',
                                default=False,
                                action='store_true',
                                help='perform action for all brands \
                                [see: config/brands-dictionary.yml]')

    # group options validate
    parser_audit.add_argument('--partner',
                              dest='PARTNER',
                              metavar='tv-brasil',
                              default='tv-brasil',
                              help='partner to perform action \
                              [see: config/brands-dictionary.yml] (default: tv-brasil)')
    parser_audit.add_argument('--all-partners',
                              dest='ALL_PARTNERS',
                              default=False,
                              action='store_true',
                              help='perform action for all partners \
                              [see: config/brands-dictionary.yml]')

    ARGS = parser.parse_args()
    ACTION = vars(ARGS)['ACTION']
    PARTNER = vars(ARGS)['PARTNER']
    ALL_PARTNERS = vars(ARGS)['ALL_PARTNERS']
    BRAND = vars(ARGS)['BRAND']
    ALL_BRANDS = vars(ARGS)['ALL_BRANDS']
    FRAMES_COUNT = vars(ARGS)['FRAMES_COUNT']

    try:
        if ACTION == 'train':
            try:
                train_brand = train.Brand(BRANDS,
                                          torch_device=CONFIG['torch_device'])
                train_brand.model()
            except Exception as error:
                logging.error('Model training fail [%s]' % error)
                raise

        elif ACTION == 'capture':

            if ALL_BRANDS:
                for brand in BRANDS:
                    capture.Images(BRANDS[brand]['id'], BRANDS[brand],
                                   frames_count=FRAMES_COUNT,
                                   sleep_time=CONFIG['capture']['sleep_time'],
                                   images_folder=CONFIG['capture']['images_folder'],
                                   brands_folder=CONFIG['brands']['folder']
                                   ).start()
            else:
                try:
                    capture.Images(BRANDS[BRAND]['id'], BRANDS[BRAND],
                                   frames_count=FRAMES_COUNT,
                                   sleep_time=CONFIG['capture']['sleep_time'],
                                   images_folder=CONFIG['capture']['images_folder'],
                                   brands_folder=CONFIG['brands']['folder']
                                   ).start()
                except Exception as error:
                    logging.error('Brand ID [%s] not found' % error)
                    raise

        elif ACTION == 'audit':

            if ALL_PARTNERS:
                for partner in BRANDS:
                    if BRANDS[partner]['audit']:
                        validate.Partners(CONFIG, BRANDS[partner]['id'], BRANDS[partner],
                                          model_folder=CONFIG['models']['folder'],
                                          db_connection_string=CONFIG['db']['path']
                                         ).start()
            else:
                try:
                    if BRANDS[PARTNER]['audit']:
                        validate.Partners(CONFIG, BRANDS[PARTNER]['id'], BRANDS[PARTNER],
                                          model_folder=CONFIG['models']['folder'],
                                          db_connection_string=CONFIG['db']['path']
                                         ).start()
                except Exception as error:
                    logging.error('Brand partner ID [%s] not found' % error)
                    raise

    except Exception as error:
        raise
