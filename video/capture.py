# -*- coding: UTF-8 -*-
'''
Application: StreamingAudit
Module: Video
description: Capture video stream as images
author: Adriano Vieira <adriano.svieira at google.com>
character encoding: UTF-8
'''

import os
from time import sleep, localtime, strftime
import cv2
import math
import numpy as np
from threading import Thread
import logging

class Images(Thread):
    def __init__(self, id, data, frames_count=10, sleep_time=5,
                 images_folder='images', brands_folder='data/brands'):
        Thread.__init__(self, name=id)
        self.id = id
        self.data = data
        self.frames_count = frames_count
        self.sleep_time = sleep_time
        self.images_folder = images_folder
        self.brands_folder = brands_folder

    def run(self):
        count = 0
        logging.info('[%s] Capturing %s images.' \
            % (self.id, self.frames_count))

        video = cv2.VideoCapture(self.data['url'])
        frame_rate = video.get(cv2.CAP_PROP_FPS)
        while video.isOpened():
            os.makedirs(self.images_folder
                        + '/' + self.id, exist_ok=True)

            frame_id = video.get(1)
            response, frame = video.read()
            if not response:
                break

            if frame_id % math.floor(frame_rate) == 0:
                img_folder = self.images_folder + '/' + self.id
                file_name = img_folder \
                            + "/img_"+strftime("%Y%m%d_%H%M%S", localtime()) \
                            + '_' +  str(int(frame_id)) + ".jpg"

                cv2.imwrite(file_name, frame)

                brands_folder = self.brands_folder \
                                + '/' + self.id + '/'
                os.makedirs(brands_folder, exist_ok=True)

                file_name_marca = brands_folder \
                                  +strftime("%Y%m%d_%H%M%S", localtime()) \
                                  +'_' +  str(int(frame_id))

                y0 = self.data['brand_xy']['y0']
                y1 = self.data['brand_xy']['y1']
                x0 = self.data['brand_xy']['x0']
                x1 = self.data['brand_xy']['x1']
                brand_right = frame[y0:y1, x0:x1]
                cv2.imwrite(file_name_marca+"_brand_right.png", brand_right)

                count += 1

                logging.info('%s' % count, end=' ', flush=True)

                if count == self.frames_count:
                    break
                else:
                    sleep(self.sleep_time)

        logging.info('\n[%s] Captured %s images.' % (self.id, count))

        video.release()
