# -*- coding: UTF-8 -*-
'''
Application: StreamingAudit
Module: Video
description: Capture frames from a video stream as images
author: Adriano Vieira <adriano.svieira at google.com>
character encoding: UTF-8
'''

import os
import cv2
import math

class BrandFrame():
    def __init__(self, id, data, frames_count=1, images_folder='images'):

        self.id = id
        self.data = data
        self.frames_count = frames_count
        self.images_folder = images_folder

    def gen_brand_frame(self):
        file_name = ''
        retval = False
        error = None
        video = cv2.VideoCapture(self.data['url'])
        try:
            count = 0
            frame_rate = video.get(cv2.CAP_PROP_FPS)
            retval = video.isOpened()
            error = 'URL_ERROR'
            while video.isOpened():
                img_folder = self.images_folder + '/' + self.id
                os.makedirs(img_folder, exist_ok=True)

                frame_id = video.get(1)
                response, frame = video.read()
                if not response:
                    break

                if frame_id % math.floor(frame_rate) == 0:
                    file_name = img_folder + "/img_" \
                                + str(int(frame_id)) \
                                + "_brand_right.png"

                    y0 = self.data['brand_xy']['y0']
                    y1 = self.data['brand_xy']['y1']
                    x0 = self.data['brand_xy']['x0']
                    x1 = self.data['brand_xy']['x1']
                    brand_right = frame[y0:y1, x0:x1]
                    cv2.imwrite(file_name, brand_right)
                    count += 1

                if count == self.frames_count:
                    break

        except Exception as e:
            error = 'CVW_ERROR'
            retval = False
            raise
        finally:
            video.release()

        return retval, error, file_name
