# -*- coding: UTF-8 -*-
'''
Application: StreamingAudit
Module: Training
description: Training to find a brand
author: Adriano Vieira <adriano.svieira at google.com>
character encoding: UTF-8
'''

from fastai import *
from fastai.vision import *
import logging

class Brand():
    def __init__(self, BRANDS_DATA,
                 BRANDS_DATA_FOLDER='data/brands', torch_device='cpu'):
        self.BRANDS_DATA = BRANDS_DATA
        self.BRANDS_DATA_FOLDER = BRANDS_DATA_FOLDER
        defaults.device = torch.device(torch_device)

    def model(self):

        # prepare to train model
        path = Path(self.BRANDS_DATA_FOLDER)
        brands = []
        logging.info("Training brands:")
        for brand in self.BRANDS_DATA:
            if self.BRANDS_DATA[brand]['train']:
                logging.info("- %s (%s)." % (self.BRANDS_DATA[brand]['name'],
                      self.BRANDS_DATA[brand]['id']))
                dest = path/self.BRANDS_DATA[brand]['id']
                dest.mkdir(parents=True, exist_ok=True)
                brands += [ self.BRANDS_DATA[brand]['id'] ]

        if brands == []:
            raise ValueError('Not found at last one brand to train')

        for brand in brands:
            verify_images(path/brand, delete=True, max_size=500)

        np.random.seed(42)
        data = ImageDataBunch.from_folder(path, train=".", valid_pct=0.2,
                                            ds_tfms=get_transforms(),
                                            size=250, num_workers=4
                                         ).normalize(imagenet_stats)

        # training model
        learn = cnn_learner(data, models.resnet34, metrics=[error_rate, accuracy])
        learn.fit_one_cycle(7)
        learn.export() # save trained model
