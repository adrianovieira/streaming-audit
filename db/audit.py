# -*- coding: UTF-8 -*-
'''
Application: StreamingAudit
Module: Validate
description: Validate check if a captured image is a brand
author: Adriano Vieira <adriano.svieira at google.com>
character encoding: UTF-8
'''

import sqlite3

class Audit():
    def __init__(self, db_connection_string='data/audit.db'):
        self.db_connection_string = db_connection_string
        try:
            self.conn = sqlite3.connect(self.db_connection_string)
            self.cursor = self.conn.cursor()

        except Exception as e:
            raise

        self.schema = 'audit(id_brand, id_partner, \
                        tm_year, tm_mon, tm_mday, \
                        tm_hour, tm_min, tm_sec, \
                        tm_wday, tm_yday, tm_isdst)'

    def close(self):
        self.conn.close()

    def insert(self, data):
        self.cursor.execute("INSERT INTO "+self.schema+" VALUES "+data)
        self.conn.commit()
