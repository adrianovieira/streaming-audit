#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
Application: StreamingAudit
Module: Database
description: Work with APP database
author: Adriano Vieira <adriano.svieira at google.com>
character encoding: UTF-8
'''

import sys
import sqlite3


if __name__ == '__main__':

    try:
        if len(sys.argv) == 2:
            conn = sqlite3.connect(sys.argv[1]+'/audit.db')
        else:
            conn = sqlite3.connect('data/audit.db')

        db_cursor = conn.cursor()

        db_cursor.execute("CREATE TABLE IF NOT EXISTS \
                                  audit(id_brand int, \
                                        id_partner int, \
                                        tm_year int, \
                                        tm_mon int, \
                                        tm_mday int, \
                                        tm_hour int, \
                                        tm_min int, \
                                        tm_sec int, \
                                        tm_wday int, \
                                        tm_yday int, \
                                        tm_isdst int)")

    except Exception as e:
        raise

    finally:
        conn.close()
