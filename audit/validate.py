# -*- coding: UTF-8 -*-
'''
Application: StreamingAudit
Module: Validate
description: Validate check if a captured image is a brand
author: Adriano Vieira <adriano.svieira at google.com>
character encoding: UTF-8
'''

from threading import Thread
from fastai import *
from fastai.vision import *
from video import brand_frame
from db import audit
import time
import logging
from datetime import datetime
from elasticsearch import Elasticsearch


class Partners(Thread):
    def __init__(self, CONFIG, partner_id, partner_data, db_connection_string,
                 model_name='export.pkl', model_folder='data/brands'):
        Thread.__init__(self, name=partner_id)
        self.partner_id = partner_id
        self.partner_data = partner_data
        self.model_name = model_name
        self.model_folder = model_folder
        self.db_connection_string = db_connection_string
        self.CONFIG = CONFIG

        # Elasticsearch
        if self.CONFIG['db']['elk']['enabled']:
            self.elk_host = self.CONFIG['db']['elk']['host']
            self.elk_host_port = self.CONFIG['db']['elk']['port']
            self.index_name = self.CONFIG['db']['elk']['index_name']
            try:
                self.elk_conn = Elasticsearch([{'host': self.elk_host, 'port': self.elk_host_port}])
                document={ 'id_brand': 'test-connect',
                           'id_partner': 'tv-brasil',
                           'name': 'Teste de conexão',
                           'timestamp': datetime.utcnow(),
                           'beat': 1, }
                self.elk_conn.index(index=self.index_name,
                                     doc_type='valida',
                                     id=1,
                                     body=document)
            except Exception as e:
                raise
            finally:
                self.elk_conn.delete(index=self.index_name, id=1)

    def run(self):
        logging.info('Auditing brands in partner ', self.partner_id)

        # prepare to check img against trained model
        trained_model_path = Path(self.model_folder)

        gf = brand_frame.BrandFrame(self.partner_id, self.partner_data)
        retval, error, img_file_name = gf.gen_brand_frame()

        if self.CONFIG['db']['sqlite']['enabled']:
            db_audit = audit.Audit(db_connection_string=self.db_connection_string)
        img = None
        if retval:
            learn = load_learner(trained_model_path, self.model_name)
            img = open_image(img_file_name)

            pred_class,pred_idx,outputs = learn.predict(img)

            logging.info('%s is broadcasting "%s" program right now.' % (self.partner_data['name'], pred_class))
            # time.struct_time:
            #    tm_year=2019, tm_mon=4, tm_mday=13,
            #    tm_hour=21, tm_min=14, tm_sec=26,
            #    tm_wday=5, tm_yday=103, tm_isdst=0
            data = '("%s", "%s", ' % (pred_class,self.partner_id) \
                   +'%s, %s, %s, %s, %s, %s, %s, %s, %s)' % time.localtime()

            document={ 'id_brand': ("%s" % pred_class),
                       'id_partner': self.partner_id,
                       'name': self.partner_data['name'],
                       'timestamp': datetime.utcnow(),
                       'beat': 1, }
        else:
            data = '("%s", "%s", ' % (error, self.partner_id) \
                    +'%s, %s, %s, %s, %s, %s, %s, %s, %s)' % time.localtime()

            document={ 'id_brand': error,
                       'id_partner': self.partner_id,
                       'name': self.partner_data['name'],
                       'timestamp': datetime.utcnow(),
                       'beat': 1, }

        if self.CONFIG['db']['elk']['enabled']:
            self.elk_conn.index(index=self.index_name,
                                 doc_type='valida',
                                 body=document)

        if self.CONFIG['db']['sqlite']['enabled']:
            db_audit.insert(data)
            db_audit.close()
